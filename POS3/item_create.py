# add / create new item

import pandas as pd
from pathlib import Path

path = Path(__file__).parent.absolute()
path2 = f'{path}/demo.xlsx'
df = pd.read_excel(path2)
df2 = pd.DataFrame({'id': [6,7], 'name': ['Kicap Cap Tamin', 'Salt'], 'price': [6.50, 3.0]})
df3 = df._append(df2)
print(df3)
# write to a excel file
path3 = f'{path}/demo.xlsx'
df3.to_excel(path3, index=False)