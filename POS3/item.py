# database of items list and dict
from pathlib import Path
import pandas as pd

class Item:
    list_items = []

    # read data from xlsx file
    def get_items(self):
        path = Path(__file__).parent.absolute()
        path2 = f'{path}/demo.xlsx'
        rows = pd.read_excel(path2) # rows = dataframe
        #print(rows)
        for row in rows.iterrows():
            data1 = row[1]
            data2 = {'id': data1['id'], 'name': data1['name'], 'price': data1['price']}
            self.list_items.append(data2)
    
    # return an item searched by id
    def find(self, id):
        for item in self.list_items:
            if item['id'] == id:
                return item
            
        return None # sama mcm null dlm php