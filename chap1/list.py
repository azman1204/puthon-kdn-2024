# list sama mcm normal array dlm PHP
names = ['abu', 'ali', 'ah chong', 'ramasamy']
print(names[1]) # ali

# update value
names[0] = 'Abu Hassan'

print(names)
# append data
names.append('john doe')
print(names)

# insert
names.insert(1, 'jane doe')
print(names)

# slice data jane ... rama
print(names[1:5])

# join list
names2 = ['jue', 'rosmah']
names.extend(names2)
print(names)

# tuples in list
profile = [('john', 'Abu'), (35, 42)]
print(profile[0][0], profile[1][0]) # john 35

# list in tuples
profile2 = (['John', 'Abu'], [35, 42])
print(profile[0][0], profile[1][0]) # john 35

# sort
names.sort() # ascending
names.sort(reverse=True) # descending
print(names)