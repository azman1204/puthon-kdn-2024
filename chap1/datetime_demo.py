from datetime import date

# weekday() - return hari dlm bentuk no. Isnin - 0, Selasa - 1, ...
print(date.today(), date.today().weekday()) # create today date
dt = date(2023, 5, 20) # create a specific date
print(dt)
today = date.today()
print(today.ctime()) # Mon July 01 03:50:00 2024

import calendar as cal
day_name = cal.day_name # [Monday, Tues, ...]
print(day_name[1])
print(day_name[today.weekday()]) # Monday

