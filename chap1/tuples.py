# tuples biasanya digunakan utk return satu var yg ada banyak data
# data dlm tuple x boleh di update / remove (immutable)
ages = (20, 25, 30)
first, second, third = ages
print(first, second, third)

nama1, nama2 = "John", "Doe"
print(nama1, nama2)

print(ages[1]) # 25