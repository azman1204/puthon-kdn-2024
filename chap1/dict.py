person = {
    'name': 'John Doe',
    'age' : 42
}
print(person.keys()) # [name, age]
print(person.values()) # ['John Doe', 42]
print(len(person)) # kira bil key:value pair
person['gender'] = 'Male' # add a new key:value
print(person)

person['name'] = 'Johan Dollah' # update value
print(person)

print(f"name is {person['name']} and age is {person['age']}")

# combine list and dictionary
person2 = [
    {'name': 'Jane Doe', 'age': 35},
    {'name': 'Abu', 'age': 46}
]

print(f'second person name is {person2[1]['name']} and his age is {person2[1]['age']}')