# string multiple lines
str = """
lines "1"
lines 2
lines '3'
"""
print(str)

str2 = 'nama saya "azman"'
print(str2.upper())
print(str2.capitalize())
print("panjang str = ", len(str))

print("index 1", str2[1]) # a
print(str2[1:4]) # range char. "ama"
print(str2[1:-2]) # -2 kira dari belakang (-1, -2, ..) "ama saya azma"