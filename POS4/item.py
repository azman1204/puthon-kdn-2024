# database of items list and dict
import sqlite3

class Item:
    list_items = []

    # read data from sqlite
    def get_items(self):
        conn = sqlite3.connect('pos.db')
        cur = conn.cursor()
        rows = cur.execute("SELECT * FROM items")
        for row in rows:
            data = {'id': row[0], 'name': row[1], 'price': row[2]}
            self.list_items.append(data)
    
    # return an item searched by id
    def find(self, id):
        for item in self.list_items:
            if item['id'] == id:
                return item
            
        return None # sama mcm null dlm php