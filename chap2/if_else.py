late = False

# format 1
if late:
    print('Yes, you are late')

# format 2
if late:
    print('Yes, you are late')
    ok = True
    if ok:
        print('ok')
else:
    print('No, you ok')

# format 3
if late:
    print('Yes, you are late')
elif 10 > 2:
    print('Yes, 10 > 2')
else:
    print('No, you ok')