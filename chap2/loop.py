person = ['John', 'Abu', 'Ali']
for position, name in enumerate(person):
    print(name, position) # John 0, Abu 1, Ali 2

for num in [1,2,3,4,5]:
    print(num)

for num in range(10): # [0,...9]
    print(num)

for num in range(3,10): # [3,...9]
    print(num)

for num in range(3,10,2): # [3,5,7,9]
    print(num)