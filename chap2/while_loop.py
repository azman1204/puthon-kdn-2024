n = 39
reminders = [] # baki [1, 1, 1, 0, 0, 1]
while n > 0:
    print(f'n={n}') # 39, 19, 9, 4, 2, 1
    reminder = n % 2
    reminders.append(reminder)
    # a = a + 5 ... a += 5
    # n = n // 2 
    n //= 2

# reminders.reverse()
print(reminders)

