# database of items list and dict

class Item:
    def get_items(self):
        return [
            {'id': 1, 'name': 'Egg',     'price': 7.80},
            {'id': 2, 'name': 'Milk',    'price': 12.50},
            {'id': 3, 'name': 'Cabbage', 'price': 4.50}
        ]
    
    # return an item searched by id
    def find(self, id):
        for item in self.get_items():
            if item['id'] == id:
                return item
            
        return None # sama mcm null dlm php