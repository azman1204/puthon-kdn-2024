from item import Item

class Purchase:
    purchase_items = []

    # id item dan qty
    def scan(self):
        while True:
            id  = input("ID : ")
            id  = int(id)

            qty = input("Qty : ")
            qty = int(qty)

            pi = {'id': id, 'qty': qty}
            self.purchase_items.append(pi)

            more = input("More items ? (y/n) : ")
            
            if more == 'n':
                break


    def print_bill(self):
        # print(self.purchase_items)
        print(f"ITEM \t\t\t PRICE \t\t QUANTITY \t\t TOTAL ROW")
        print('-' * 75)
        itm = Item()
        total = 0
        for item in self.purchase_items:
            id  = item['id']
            qty = item['qty']
            item2 = itm.find(id)
            total_row = qty * item2['price']
            total += total_row
            print(f"{item2['name']} \t\t\t {item2['price']} \t\t {qty} \t\t {total_row:.2f}")
        
        sst = 0.07 * total
        total += sst
        print('-' * 75)
        print(f"SST   : {sst:.2f}")
        print(f"TOTAL : {total:.2f}")