class StringUtils:
    # static property
    _name = 'John Doe'

    @classmethod
    @property
    def name(cls):
        return cls._name

    @staticmethod
    def loremipsum():
        str = "lorem ipsum dolar sit amet..."
        return str
    
# util = StringUtils() # instantiate
str = StringUtils.loremipsum()
print(str)
print(StringUtils.name)