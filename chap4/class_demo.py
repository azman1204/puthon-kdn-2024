
# () guna utk inheritance
class Simplest():
    pass

class Animal():
    # properties
    name = 'Animal planet'

class Monkey(Animal):
    # properties
    breed = 'Mawas'

    # methods
    # semua methods dlm class mesti ada first param 'self'
    # dlm php '$this'
    def info(self):
        return f"name: {self.name}, breed: {self.breed}" 


# create object dari class
monkey = Monkey()
print(monkey.name, monkey.breed)

monkey.breed = 'Lutong' # update value
print(monkey.name, monkey.breed)

print(monkey.info())