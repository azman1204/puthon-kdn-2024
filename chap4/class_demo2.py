class Rectangular():
    # constructor
    def __init__(self, side_a, side_b):
        # initialize obj properties
        self.side_a = side_a
        self.side_b = side_b

    # kira luas empat segi
    def area(self):
        return self.side_a * self.side_b
    


rect = Rectangular(side_a=10, side_b=4)
print("Luas = ", rect.area())
