import mysql.connector
from mysql.connector import Error

try:
    conn = mysql.connector.connect(
        host='localhost',
        database='pos',
        user='root',
        password='',
        port='3307'
    )

    if conn.is_connected():
        print('Success')
        cur = conn.cursor(dictionary=True) # return data as dic instead of list
        cur.execute("SELECT * FROM items")
        rows = cur.fetchall()
        print(rows)
        for row in rows:
            # print(f"id: {row[0]} name: {row[1]} price: {row[2]}")
            print(f"id: {row['id']} name: {row['name']} price: {row['price']}")

except Error as e:
    print(f'Error: {e}')    
finally:
    # always run. ada error or tiada error
    cur.close()
    conn.close()