from item import Item

class Purchase:
    purchase_items = []

    # id item dan qty
    def scan(self):
        while True:
            id  = input("ID : ")
            id  = int(id)

            qty = input("Qty : ")
            qty = int(qty)

            pi = {'id': id, 'qty': qty}
            self.purchase_items.append(pi)

            more = input("More items ? (y/n) : ")
            
            if more == 'n':
                break


    def print_bill(self):
        # print(self.purchase_items)
        print("ITEM".ljust(15) + "|", end='')
        print("PRICE".ljust(10) + "|", end='')
        print("QUANTITY".ljust(10) + "|", end='')
        print("TOTAL ROW".ljust(10) + "|")
        print('-' * 48 + "|")
        itm = Item()
        itm.get_items()
        total = 0
        for item in self.purchase_items:
            id  = item['id']
            qty = item['qty']
            item2 = itm.find(id)
            total_row = qty * item2['price']
            total += total_row
            
            name = str(item2['name'][0:15])
            print(name.ljust(15) + "|", end='')

            price = str(item2['price'])
            print(price.ljust(10) + "|", end='')

            qty2 = str(qty)
            print(qty2.ljust(10) + "|", end='')

            total_row2 = f'{total_row:.2f}'
            print(total_row2.ljust(10) + "|")
        
        sst = 0.07 * total
        total += sst
        print('-' * 48 + "|")
        print(f"SST   : {sst:.2f}")
        print(f"TOTAL : {total:.2f}")