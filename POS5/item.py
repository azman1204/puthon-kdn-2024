# database of items list and dict
import requests

class Item:
    list_items = []

    # read data from sqlite
    def get_items(self):
        x = requests.get('http://localhost:8000/pos')
        # print(x.json())
        # print(type(x.json())) # json() - convert json string into list and dict
        rows = x.json()
        self.list_items = rows
    
    # return an item searched by id
    def find(self, id):
        for item in self.list_items:
            if item['id'] == id:
                return item
            
        return None # sama mcm null dlm php