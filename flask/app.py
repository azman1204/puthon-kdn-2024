from flask import Flask, render_template

app = Flask(__name__) # __main__ bermaksud kita run directly dari file ini

# route = URL path http://localhost:500/about

# default path http://localhost:500/
@app.route("/")
def hello_world():
    return "<p>Hello World</p>"


# http://localhost:500/about
@app.route("/about")
def about():
    return "<h1>About me...</h1>"


# http://localhost:500/product
@app.route("/product")
def product():
    # templates/product.html
    products = [
        {'name': 'Python 3.12', 'price': 0.00},
        {'name': 'Pandas', 'price': 1000}
    ]
    return render_template(
        'product.html', 
        name="John Doe",
        prods=products
    )

# http://localhost:500/product/2
@app.route("/product/<id>")
def detail(id):
    id = int(id) # convert string to int
    # templates/product.html
    products = [
        {'id': 1, 'name': 'Python 3.12', 'price': 0.00},
        {'id': 2, 'name': 'Pandas', 'price': 1000}
    ]
    
    prod = None
    for p in products:
        if p['id'] == id:
            prod = p

    return render_template(
        'product_detail.html', 
        prod=prod
    )