import sqlite3

conn = sqlite3.connect('mydb.db')
cur = conn.cursor()
rows = cur.execute("SELECT * FROM users")
for row in rows:
    print(f'id: {row[0]} name: {row[1]} age: {row[2]}')

conn.commit()
conn.close()