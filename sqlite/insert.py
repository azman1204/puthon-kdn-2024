import sqlite3

conn = sqlite3.connect('mydb.db')
cur = conn.cursor()
cur.execute("INSERT INTO users(id, name, age) VALUES(1, 'John Doe',     42)")
cur.execute("INSERT INTO users(id, name, age) VALUES(2, 'Ali Bakar',    40)")
cur.execute("INSERT INTO users(id, name, age) VALUES(3, 'Ahmad hassab', 45)")
conn.commit()
conn.close()