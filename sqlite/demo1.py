import sqlite3

conn = sqlite3.connect('mydb.db')
cur = conn.cursor()
cur.execute('''
            CREATE TABLE users(
                id integer primary key,
                name text,
                age integer
            )
            ''')
conn.commit()
conn.close()