from fastapi import FastAPI
from pos import pos_router
from demo import demo_router

app = FastAPI()
app.include_router(pos_router, prefix='/pos')
app.include_router(demo_router, prefix='/demo')