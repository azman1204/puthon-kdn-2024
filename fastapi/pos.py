from fastapi import APIRouter
import mysql.connector
from mysql.connector import Error
from models.item import Item

pos_router = APIRouter()

# return list of all itemsdari table items dlm db pos of MySQL
@pos_router.get("/")
def read_items():
    try:
        conn = mysql.connector.connect(host='localhost',database='pos',user='root',password='',port='3307')
        if conn.is_connected():
            cur = conn.cursor(dictionary=True)
            cur.execute("SELECT * FROM items")
            rows = cur.fetchall()
            data = []
            for row in rows:
                data.append(row)
            return data
    except Error as e:
        return {"error": "Connection to MySQL problem"}   
    finally:
        cur.close()
        conn.close()

# http://localhost:8000/pos/item
@pos_router.post("/item")
def save_item(item: Item):
    try:
        conn = mysql.connector.connect(host='localhost',database='pos',user='root',password='',port='3307')
        if conn.is_connected():
            cur = conn.cursor(dictionary=True)
            cur.execute(f"INSERT INTO items(id, name, price) VALUES({item.id}, '{item.name}', {item.price})")
            conn.commit()
            return item
    except Error as e:
        return {"error": "Connection to MySQL problem"}   
    finally:
        cur.close()
        conn.close()