from pydantic import BaseModel
from typing import Union
from fastapi import APIRouter

demo_router = APIRouter()

class Item(BaseModel):
    name: str
    price: float
    is_offer: Union[bool, None] = None

@demo_router.get("/")
def read_root():
    return {"Hello": "World"} # FastAPI auto convert dict to JSON


# http://localhost:8000/demo/items/5?q=abu
@demo_router.get("/items/{item_id}")
def read_item(item_id: int, q:Union[str, None] = None):
    return {"item_id": item_id, "q": q}


@demo_router.put("/items/{item_id}")
def update_item(item_id: int, item: Item):
    return {"item_name": item.name, "item_id": item_id}