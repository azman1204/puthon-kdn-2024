# file yg ada function dan class, potensi jadi module
# define a function
def my_function():
    print('This is my function')

my_function() # call the function


# function with parameter
def tambah(no1, no2):
    no3 = no1 + no2
    return no3

no4 = tambah(10, 5)
print(no4)

# function with optional parameter. param yg optional mesti datang selepas yg wajib
# selepas optional, dah x boleh ada yg wajib
def tolak(no1, no2 = 0):
    no3 = no1 - no2
    return no3

no4 = tolak(10) # 10 - 0 = 10
no5 = tolak(10, 5) # 10 - 5 = 5
print(f"no4 = {no4}, no5 = {no5}")

# call function using parameter name
no6 = tolak(no1=10, no2=3)
print("no6 = ", no6) # 10 - 3 = 7

no7 = tolak(no2=3, no1=10) # 10 - 3
print("no7 = ", no7) # 10 - 3 = 7

# function yg return multiple value
def darab(no1, no2 = 1):
    no3 = no1 * no2
    return (no1, no2, no3)

no1, no2, no3 = darab(10, 2)
print(no1, no2, no3)