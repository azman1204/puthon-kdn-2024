# : data type - hinting
def huruf_besar(word: str):
    return word.upper()

def huruf_kecil(word: str):
    return word.lower()