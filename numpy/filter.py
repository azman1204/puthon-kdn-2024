import numpy

a = numpy.array([1,2,3,4,5,6,7,8,9,10])
b = a > 5
# print(b) # [False False False False False  True  True  True  True  True]
# print(a[b]) # [ 6  7  8  9 10]

# print no 3 dan 7 shj
# print(a[[False, False, True, False, False, False, True, False, False, False]]) # [3 7]

# print no genap shj
b = a % 2 == 0
print(a[b])