import numpy

# array 1-dimensi
arr = numpy.array([1, 2, 3, 4, 5, 6, 7])
# print(arr[1:5])   # [2,3,4,5]
# print(arr[4:])    # [5,6,7]
# print(arr[-3:-1]) # [5,6]
# print(arr[1:5:2]) # [2,4] - 2 = step

# array 2-dimensi
arr = numpy.array([
    [1,2,3], 
    [4,5,6]
]) 
# print(arr[1,0]) # row,col 4

# print semua data dari row kedua
# print(arr[1,]) # [4,5,6]

# print semua data dari col ke-2
# print(arr[:,1]) # [2,5]

# print semua data dr col-1 dan col-3
print(arr[:, [0,2]]) # [[1,4], [3,6]]

# arra 3-dimensi
arr = numpy.array([
    [
        [1,2,3],[4,5,6]
    ],
    [
        [7,8,9],[10,11,12]
    ]
])