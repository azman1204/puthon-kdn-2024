from pathlib import Path # see page 331

# __file__ = C:/Users/azman/Desktop/2024/KDN/tot/chap5/read_file.py
path = Path(__file__).parent.absolute()
# print(path)
path2 = f'{path}/data.txt'
# exit()

# path = 'C:/Users/azman/Desktop/2024/KDN/tot/chap5/'
fh = open(path2, '+a') # read / write (append)

# write to a file
fh.write("\nHello")
fh.close()

# read a file content
fh = open(path2, 'rt')
for line in fh.readlines():
    print(line.strip())

fh.close()