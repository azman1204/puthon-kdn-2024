# database of items list and dict
from pathlib import Path

class Item:
    list_items = []

    def get_items(self):
        path = Path(__file__).parent.absolute()
        path2 = f'{path}/demo.csv'
        fh = open(path2, 'rt')
        bil = 0
        for data in fh.readlines():
            # data = 1,Milk,12.50
            if bil == 0:
                bil += 1
                continue # skip header

            # data2 = [1, 'milk', 12.50]
            # split() sama mcm php explode() - return list / array
            data2 = data.strip().split(',') 
            item = {'id': int(data2[0]), 'name': data2[1], 'price': float(data2[2])}
            self.list_items.append(item)
    
    # return an item searched by id
    def find(self, id):
        for item in self.list_items:
            if item['id'] == id:
                return item
            
        return None # sama mcm null dlm php